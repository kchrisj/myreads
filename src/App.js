import React from 'react'
import {Route} from 'react-router-dom'
import * as BooksAPI from './BooksAPI'
import Shelves from './Shelves'
import Search from './Search'
import './App.css'

class BooksApp extends React.Component {
  state = {
    //showSearchPage: false,
    books: [],
  }

  componentDidMount(){
    BooksAPI.getAll().then((books)=>{
      this.setState({books: books})
    })
  }

  moveAbook = (book, shelf)=>{
    console.log(book.shelf, shelf)
    book.shelf = shelf
    console.log(book.shelf)
    BooksAPI.update(book, shelf)
    .then (res => console.log(res))
    .then(()=>{BooksAPI.getAll()
    .then((books)=>{this.setState({books: books})})
    .then((books) => console.log(book))
    })
  }

  //moveAbook = (book, shelf)=>{
  //  book.shelf = shelf
  //  BooksAPI.update(book, shelf)
  //    .then(()=>{
  //      this.setState(state=>({books:state.book
  //      .filter(b=>b.id != book)
  //      .concat(book)
  //    }))
  //    })
  //}

  render() {
    return (
      <div className="app">
        <Route exact path='/' render={() => (<Shelves books = {this.state.books} moveAbook={this.moveAbook} />)} />
        <Route path='/search' render={() => ( <Search books = {this.state.books} moveAbook={this.moveAbook} />)} />

      </div>
    )
  }
}

export default BooksApp
