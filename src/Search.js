import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import * as BooksAPI from './BooksAPI'
import Book from './Book'
import Shelves from './Shelves'

class Search extends Component {


  state = {
    searchResults: [],
  }

  bookSearch = (event) => {
    if(event.target.value ===""){
      this.setState({searchResults:[]})
    }else{
      BooksAPI.search(event.target.value)
      .then((results)=>{
        if(results.error !== 'empty error'){
          this.setState({searchResults:this.whichShelf(results)})
          this.whichShelf(results)
        }
        else{
          this.setState({searchResults:[]})
          //console.log(this.state.searchResults);
        }
      })
      .catch(error => console.log(error))
    }

  }

  whichShelf(searchResults){
    return searchResults.map(searchedBook => {
      searchedBook.shelf = 'none'
      this.props.books.forEach(currentBook =>{
        searchedBook.id === currentBook.id && (searchedBook.shelf = currentBook.shelf)
      })
      return searchedBook
    })
  }

  render(){
    return(
        <div className="search-books">
          <div className="search-books-bar">
            <Link to = "/" className="close-search">Close</Link>
              <div className="search-books-input-wrapper">
                <input
                  type="text" placeholder="Search by title or author"
                  onChange= {this.bookSearch}
                />
              </div>
          </div>
          <div className="search-books-results">
            <ol className="books-grid"></ol>
            {this.state.searchResults.length > 0 ?
              this.state.searchResults.map((book)=>{
                return <li key={book.id}><Book book={book} moveAbook={this.props.moveAbook} /></li>
              }):
              <h2>No such book</h2>
            }
          </div>
        </div>
    )

  }

}
export default Search
