import React from 'react'
import './App.css'

function Book (props) {
    //console.log('bookinfo: ', props.book)
    //console.log('shelf: ', props.shelf)
    //console.log(props.moveAbook)
    //console.log(props);
    return (
      <div>

            <div className="book">
              <div className="book-top">
                <div className="book-cover" style={{ width: 128, height: 193, backgroundImage: `url("${props.book.imageLinks ? props.book.imageLinks.thumbnail : 'https://imgur.com/ZHDkYoR.jpg'}")`}}></div>
                <div className="book-shelf-changer">
                  <select onChange={(event) => props.moveAbook(props.book, event.target.value)} value = {props.book.shelf}>
                    <option value="move" disabled>Move to...</option>
                    <option value="currentlyReading">Currently Reading</option>
                    <option value="wantToRead">Want to Read</option>
                    <option value="read">Read</option>
                    <option value="none">None</option>
                  </select>
                </div>
              </div>
              <div className="book-title">{props.book.title}</div>
              <div className="book-authors">{props.book.authors}</div>
            </div>

    </div>
  )
}
export default Book
