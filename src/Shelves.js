import React from 'react'
import Book from './Book'
import { Link } from 'react-router-dom'


/*
Array of Book objects from Apps.js passed to shelves stateless function
Create an array of the shelves and iterate( via map method) over them to display
Note {}' around mapping method
*/

 function Shelves(props) {
     //{props.books.map(book => console.log(book.id))}
     //console.log(props.moveAbook)
    const shelves = [{'id':"CR", 'shlf':'Currently Reading', s:'currentlyReading'},
                    {'id':"WTR", 'shlf':"Want To Read", s:'wantToRead'},
                    {'id':"WR", 'shlf':"Read", s:'read'}];
        return (
            <div>
                <div className="list-books">
                  <div className="list-books-title">
                    <h1>MyReads</h1>
                  </div>
                  <div className="list-books-content">
                      {shelves.map(shelf => {
                        return(
                            <div className='bookshelf' key={shelf.id}>
                              <h2 className="bookshelf-title">{shelf.shlf}</h2>
                              <div className="bookshelf-books">
                                <ol className="books-grid">
                                    {props.books.map(book => {
                                        if (book.shelf === shelf.s) {
                                            return <li key={book.id}><Book book={book} shelf={shelf.s}  moveAbook={props.moveAbook} /></li>
                                        }
                                    })}
                                </ol>
                              </div>
                            </div>
                        )
                    })}
                  </div>
                  <div className="open-search">
                    <Link to = '/search'>Add a book</Link>
                  </div>
              </div>
            </div>
        )
}
export default Shelves
