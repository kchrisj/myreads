# MyReads Project

This an interactive application for managing your book collection.

To get started:
1 unzip the project files
2 be sure to install all project dependencies INSIDE the project   
  directory with `npm install`
3 start server with `npm start`

## What You're Getting
```bash
├── CONTRIBUTING.md
├── README.md - This file.
├── SEARCH_TERMS.md # The whitelisted short collection of available search terms for you to use with your app.
├── package.json # npm package manager file. It's unlikely that you'll need to modify this.
├── public
│   ├── favicon.ico # React Icon, You may change if you wish.
│   └── index.html # DO NOT MODIFY
└── src
    ├── App.css # Styles for your app. Feel free to customize this as you desire.
    ├── App.js # This is the root of your app. Contains static HTML right now.
    ├── App.test.js # Used for testing. Provided with Create React App. Testing is encouraged, but not required.
    |__ Books.js #
    |__ Search.js #
    |__ Shelves.js #
    ├── BooksAPI.js # A JavaScript API for the provided Udacity backend. Instructions for the methods are below.
    ├── icons # Helpful images for your app. Use at your discretion.
    │   ├── add.svg
    │   ├── arrow-back.svg
    │   └── arrow-drop-down.svg
    ├── index.css # Global styles. You probably won't need to change anything here.
    └── index.js # You should not need to modify this file. It is used for DOM rendering only.
```

* Books are on one of three shelves, Read, Currently Reading and Want to Want to Read.
* To Assign a book to a shelf, click on the green button and choose the appropriate shelf
* To look for a book, click on the green button with the plus in the lower right. Folloe
   instructions

## Contributing

For details, check out [CONTRIBUTING.md](CONTRIBUTING.md).
